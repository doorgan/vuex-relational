// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import store from './store'
import ORM from './lib/orm'
import User from './models/User'
import Post from './models/Post'

/**
 * Register ORM models
 */
Vue.use(ORM, {
  store: store,
  models: [User, Post]
})

Vue.config.productionTip = false

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  template: '<App/>',
  components: { App }
})
