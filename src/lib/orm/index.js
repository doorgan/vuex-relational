import ORM from './ORM'

export default {
  install (Vue, options) {
    if (!options.store) {
      throw new Error('You must provide a valid Vuex store')
    }

    Vue.registerModels = (models) => {
      var orm = new ORM(options.store)
      orm.registerModels(models)
    }

    Vue.registerModel = (model) => {
      var orm = new ORM(options.store)
      orm.registerModel(model)
    }

    if (options.models) {
      Vue.registerModels(options.models)
    }
  }
}
