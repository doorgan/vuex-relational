import store from '../../store'

class Model {

  /**
   * Constructs the object.
   *
   * @param      data     Model data
   * @param      options  The options
   */
  constructor (data) {
    this._store = store
    this._namespace = this.constructor.name.toLowerCase() + 's'

    var validateSchema = this.schema(data)
    if (validateSchema) {
      this.data = data
    } else {
      throw this.schema.errors(data)
    }
  }

  /**
   * Getter
   */
  get (property) {
    return this.data[property]
  }

  /**
   * Setter
   */
  set (property, value) {
    this.data[property] = value
  }

  /**
   * Returns all items in the store
   */
  all () {
    return this._store.state[this._namespace].all
  }

  /**
   * Dispatchs a namespaced action in the store
   */
  dispatch (action, payload = {}) {
    payload.namespace = this._namespace
    this._store.dispatch(this._namespace + '/' + action, payload)
  }

  /**
   * Returns the object index in the store
   */
  getIndex () {
    var all = this.all()
    var index = all.findIndex(element => {
      return typeof element !== undefined && element.id === this.data.id
    })
    if (index > -1) {
      return index
    } else {
      return null
    }
  }

  /**
   * Belongs to relationship definition
   *
   * @param      namespace  The namespace
   * @param      key        The object foreign key
   * @return     Model
   */
  belongsTo (namespace, key) {
    var relation = this._store.state[namespace].all.filter(obj => {
      return obj.id === this.data[key]
    })[0]
    if (relation) {
      return relation
    } else {
      throw new Error('Relationship ' + namespace + ' does not exist')
    }
  }

  /**
   * Deletes object data from the store
   */
  destroy () {
    var index = this.getIndex()
    if (index != null) {
      this.dispatch('deleteById', {id: this.data.id})
    }
  }

  /**
   * Inserts or updates the object in the store
   */
  save () {
    // First we check if this instance is saved in the state
    var index = this.getIndex()
    if (index != null) {
      // The object exists so we replace it
      this.dispatch('update', {id: this.id, data: this.data})
    } else {
      // The object does not exist so we push it
      this.dispatch('insert', {data: this.data})
    }
  }
}

export default Model
