import module from './store/module'

class ORM {
  /**
   * Constructs the object.
   *
   * @param      store   The store to register modules
   */
  constructor (store) {
    this._store = store
  }

  /**
   * Registers the modules to the store
   *
   * @param      Array  models  The models
   */
  registerModels (models = []) {
    for (var i = 0; i < models.length; i++) {
      this._store.registerModule(models[i].name.toLowerCase() + 's', module)
    }
  }

  /**
   * Register a single model
   *
   * @param      Model  model   The model
   */
  registerModel (model) {
    this._store.registerModule(model.name.toLowerCase() + 's', module)
  }
}

export default ORM
