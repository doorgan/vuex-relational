export const mutations = {
  insert (state, payload) {
    console.log(JSON.parse(JSON.stringify(state)))
    console.log(payload)
    state.all.push(payload.data)
  },
  update (state, payload) {
    var items = state.all.filter(obj => {
      return obj.id === payload.id
    })
    for (var i = 0; i < items.length; i++) {
      var index = state.all.indexOf(items[i])
      state.all[index] = payload.data
    }
  },
  deleteByIndex (state, payload) {
    state.all.splice(payload.index, 1)
  },
  deleteById (state, payload) {
    var items = state.all.filter(obj => {
      return obj.id === payload.id
    })
    for (var i = 0; i < items.length; i++) {
      var index = state.all.indexOf(items[i])
      state.all.splice(index, 1)
    }
  }
}
