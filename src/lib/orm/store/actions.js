export const actions = {
  insert ({commit}, payload) {
    commit('insert', payload)
  },
  update ({commit}, payload) {
    commit('update', payload)
  },
  deleteByIndex ({commit}, payload) {
    commit('deleteByIndex', payload)
  },
  deleteById ({commit}, payload) {
    commit('deleteById', payload)
  }
}
