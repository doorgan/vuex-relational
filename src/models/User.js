import Model from '../lib/orm/Model'
import schema from 'js-schema'

class User extends Model {
  /**
   * User schema
   */
  get schema () {
    return schema({
      'id': Number,
      'name': String
    })
  }
}

export default User
