import Model from '../lib/orm/Model'
import schema from 'js-schema'

class Post extends Model {
  /**
   * Post schema
   */
  get schema () {
    return schema({
      'id': Number,
      'user_id': Number,
      '?body': String
    })
  }

  /**
   * User relationship
   */
  get user () {
    return this.belongsTo('users', 'user_id')
  }
}

export default Post
